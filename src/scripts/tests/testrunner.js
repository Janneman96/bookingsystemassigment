var testRunner = {
  /**
   * Displays a success message in the console
   * @param {string} message Message to display
   * 
   * @returns {void}
   */
  success(message) {
    console.log('%cTest succes: ' + message, 'color: green;');
  },

  /**
   * Displays an error message in the console
   * @param {string} name Test function name
   * @param {string} reason Reason of the test failing
   * 
   * @returns {void}
   */
  error(name, reason) {
    console.error('Test failed: ' + name + '\n', reason);
  },

  /**
   * Runs all tests
   */
  runAll(tests) {
    console.log('Running all tests...');
    var succeeded = 0;
    var failed = 0;
    for (var property in tests) {
      var testGroup = tests[property];
      console.log('%c' + property, 'font-weight: bold; font-size: 16px;');
      for (var property in testGroup) {
        var response = testGroup[property]();
        if (response) {
          this.error(property, response);
          failed++;
        } else {
          this.success(property);
          succeeded++;
        }
      }
    }

    console.log('%cResults', 'font-weight: bold; font-size:20px;');

    if (!failed) {
      console.log('%cAll ' + succeeded + ' tests have succeeded', 'color: green; font-size:16px;');
    } else {
      console.error('%c' + failed + ' tests have failed', 'font-size: 16px;');
    }
  }
}

export default testRunner;