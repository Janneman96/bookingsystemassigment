import validationTests from './booking/validationTests.js';
import acceptanceTests from './booking/acceptanceTests.js';
import priceTests from './booking/priceTests.js';

var bookingServiceTests = {
  bookingAcceptanceTests: acceptanceTests,
  bookingValidationTests: validationTests,
  bookingPriceTests: priceTests
}

export default bookingServiceTests;