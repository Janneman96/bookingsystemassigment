import bookingService from '/scripts/booking/bookingService.js';

/**
 * Contains all tests for total price of a booking.
 */
var priceTests = {
  returnsCorrectPriceForOneHourBookingOnFridayAfternoon() {
    // Arrange
    var expectedPrice = 38;
    var startDate = '2019-07-05T12:00+11:00';
    var endDate = '2019-07-05T13:00+11:00';

    // Act
    var output = bookingService.getPrice(startDate, endDate, dummyRates);

    // Assert
    if (output !== expectedPrice) return 'A one hour booking on friday should cost €' + expectedPrice + '. The calculated price was €' + output + '.';
  },
  returnsCorrectPriceForTwoHourBookingOnFridayNight() {
    // Arrange
    var expectedPrice = 85.86;
    var startDate = '2019-07-05T03:00+11:00';
    var endDate = '2019-07-05T05:00+11:00';

    // Act
    var output = bookingService.getPrice(startDate, endDate, dummyRates);

    // Assert
    if (output !== expectedPrice) return 'A two hour booking on friday night should cost €' + expectedPrice + '. The calculated price was €' + output + '.';
  },
  returnsCorrectPriceForNightAndMorningOverlapping() {
    // Arrange
    var expectedPrice = 171.72;
    var startDate = '2019-07-05T04:00+11:00';
    var endDate = '2019-07-05T08:00+11:00';

    // Act
    var output = bookingService.getPrice(startDate, endDate, dummyRates);

    // Assert
    if (output !== expectedPrice) return 'A four hour booking on friday night and morning should cost €' + expectedPrice + '. The calculated price was €' + output + '.';
  },
  returnsCorrectPriceForAfternoonAndNightOverlapping() {
    // Arrange
    var expectedPrice = 171.72;
    var startDate = '2019-07-05T18:00+11:00';
    var endDate = '2019-07-05T22:00+11:00';

    // Act
    var output = bookingService.getPrice(startDate, endDate, dummyRates);

    // Assert
    if (output !== expectedPrice) return 'A four hour booking on friday afternoon and night should cost €' + expectedPrice + '. The calculated price was €' + output + '.';
  },
  returnsCorrectPriceForRateWithinTimespan() {
    // Arrange
    var expectedPrice = 120;
    var startDate = '2019-07-05T07:00+11:00';
    var endDate = '2019-07-05T10:00+11:00';

    // Act
    var output = bookingService.getPrice(startDate, endDate, dummyRates);

    // Assert
    if (output !== expectedPrice) return 'A three hour booking on friday surrounding a more expensive rate should cost €' + expectedPrice + '. The calculated price was €' + output + '.';
  },
  returnsRoundedDownPrice() {
    // Arrange
    var expectedPrice = 53.66;
    var startDate = '2019-07-05T03:00+11:00';
    var endDate = '2019-07-05T04:15+11:00';

    // Act
    var output = bookingService.getPrice(startDate, endDate, dummyRates);

    // Assert
    if (output !== expectedPrice) return 'The price should be rounded down to two decimals. The expected price was €' + expectedPrice + '. The calculated price was €' + output + '.';
  },
  returnsCorrectPriceForRateWithinTimespanSpanningTwoDays() {
    // Arrange
    var expectedPrice = 515.16;
    var startDate = '2017-10-18T18:00:00+11:00';
    var endDate = '2017-10-19T06:00:00+11:00';

    // Act
    var output = bookingService.getPrice(startDate, endDate, dummyRates);

    // Assert
    if (output !== expectedPrice) return 'A four hour booking on friday afternoon and night should cost €' + expectedPrice + '. The calculated price was €' + output + '.';
  }
}

var dummyRates = [
  {
    days: [1, 2, 3, 4, 5],
    startHour: 0,
    endHour: 5,
    rate: 42.93
  },
  {
    days: [1, 2, 3, 4, 5],
    startHour: 6,
    endHour: 19,
    rate: 38
  },
  {
    days: [1, 2, 3, 4, 5],
    startHour: 20,
    endHour: 23,
    rate: 42.93
  },
  {
    days: [6],
    startHour: 0,
    endHour: 23,
    rate: 45.91
  },
  {
    days: [7],
    startHour: 0,
    endHour: 23,
    rate: 60.85
  },
  {
    days: [5],
    startHour: 8,
    endHour: 9,
    rate: 40
  }
];

export default priceTests;