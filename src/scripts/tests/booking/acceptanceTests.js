import input from '/scripts/data/input.js';
import expectedOutput from '/scripts/data/resultaat.js';

import bookingService from '/scripts/booking/bookingService.js';

/**
 * Contains the acceptance tests of the booking system.
 */
var acceptanceTests = {
  inputAndExpectedOutputMatch() {
    // Act
    var output = bookingService.processAllBookings(input);

    // Assert
    if (JSON.stringify(output) != JSON.stringify(expectedOutput)) {
      console.log('expected', expectedOutput);
      console.log('actual', output);
      return 'The bookings were not mutated properly.';
    }
  }
}

export default acceptanceTests;