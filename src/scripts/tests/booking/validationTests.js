import bookingService from '/scripts/booking/bookingService.js';

/**
 * Contains all tests for the start and end date of a booking.
 */
var validationTests = {
  acceptsBooking1Hour() {
    // Arrange
    var startDate = '2017-10-23T08:00:00+11:00';
    var endDate = '2017-10-23T09:00:00+11:00';

    // Act
    var output = bookingService.validate(startDate, endDate);

    // Assert
    if (output !== true) return 'A one hour booking should be accepted.';
  },

  acceptsBooking2Hour() {
    // Arrange
    var startDate = '2017-10-23T08:00:00+11:00';
    var endDate = '2017-10-23T10:00:00+11:00';

    // Act
    var output = bookingService.validate(startDate, endDate);

    // Assert
    if (output !== true) return 'A two hour booking should be accepted.';
  },

  rejectsBookingHalfHour() {
    // Arrange
    var startDate = '2017-10-23T08:00:00+11:00';
    var endDate = '2017-10-23T08:30:00+11:00';

    // Act
    var output = bookingService.validate(startDate, endDate);

    // Assert
    if (output !== false) return 'A booking shorter than one hour should be rejected.';
  },

  rejectsBooking25Hours() {
    // Arrange
    var startDate = '2017-10-23T08:00:00+11:00';
    var endDate = '2017-10-24T09:00:00+11:00';

    // Act
    var output = bookingService.validate(startDate, endDate);

    // Assert
    if (output !== false) return 'A booking longer than 24 hours should be rejected.';
  },

  acceptsBooking24Hours() {
    // Arrange
    var startDate = '2017-10-23T08:00:00+11:00';
    var endDate = '2017-10-24T08:00:00+11:00';

    // Act
    var output = bookingService.validate(startDate, endDate);

    // Assert
    if (output !== true) return 'A booking of exactly 24 hours should be allowed.';
  },

  rejectsEndDateBeforeStartDate() {
    // Arrange
    var startDate = '2017-10-23T09:00:00+11:00';
    var endDate = '2017-10-23T08:00:00+11:00';

    // Act
    var output = bookingService.validate(startDate, endDate);

    // Assert
    if (output !== false) return 'The end date should come after the start date.';
  },

  acceptsBooking1Hour15Minutes() {
    // Arrange
    var startDate = '2017-10-23T09:00:00+11:00';
    var endDate = '2017-10-23T10:15:00+11:00';

    // Act
    var output = bookingService.validate(startDate, endDate);

    // Assert
    if (output !== true) return 'A booking of one hour and fifteen seconds should be accepted.';
  },

  rejectsBooking1Hour8Minutes() {
    // Arrange
    var startDate = '2017-10-23T09:00:00+11:00';
    var endDate = '2017-10-23T10:08:00+11:00';

    // Act
    var output = bookingService.validate(startDate, endDate);

    // Assert
    if (output !== false) return 'The timespan of a booking should be a multiple of 15 minutes.';
  },

  rejectsDifferentTimezones() {
    // Arrange
    var startDate = '2017-10-18T18:00:00+11:00';
    var endDate = '2017-10-18T19:00:00-11:00';

    // Act
    var output = bookingService.validate(startDate, endDate);

    // Assert
    if (output !== false) return 'A booking with a start date and end date in different time zones should be rejected.';
  },

  rejectsMinutesNotMultipleOf15() {
    // Arrange
    var startDate = '2017-10-23T09:08:00+11:00';
    var endDate = '2017-10-23T10:08:00+11:00';

    // Act
    var output = bookingService.validate(startDate, endDate);

    // Assert
    if (output !== false) return 'The minutes in the end date of the booking should be a multiple of 15. Allowed minutes: 00, 15, 30, 45.';
  }
}

export default validationTests;