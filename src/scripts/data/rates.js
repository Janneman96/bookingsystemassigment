export default [
  {
    days: [1, 2, 3, 4, 5],
    startHour: 0,
    endHour: 5,
    rate: 42.93
  },
  {
    days: [1, 2, 3, 4, 5],
    startHour: 6,
    endHour: 19,
    rate: 38
  },
  {
    days: [1, 2, 3, 4, 5],
    startHour: 20,
    endHour: 23,
    rate: 42.93
  },
  {
    days: [6],
    startHour: 0,
    endHour: 23,
    rate: 45.91
  },
  {
    days: [7],
    startHour: 0,
    endHour: 23,
    rate: 60.85
  }
];