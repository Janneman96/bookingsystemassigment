import constants from '/scripts/data/constants.js';

var booking = {
  startDate: undefined,
  endDate: undefined
}

var bookingValidator = {
  validate(startDate, endDate) {
    booking.startDate = startDate;
    booking.endDate = endDate;

    var isValid =
      isAtLeast1Hour() &&
      !isOver24Hours() &&
      durationIsMultipleOf15Minutes() &&
      endTimeIsMultipleOf15Minutes();

    return isValid;
  },
}

function isAtLeast1Hour() {
  return getTimeSpan() >= constants.TIMESPAN_OF_ONE_HOUR;
}

function isOver24Hours() {
  return getTimeSpan() > constants.TIMESPAN_OF_ONE_HOUR * 24;
}

function durationIsMultipleOf15Minutes() {
  var fifteenMinutes = constants.TIMESPAN_OF_ONE_HOUR / 4;
  return getTimeSpan() % fifteenMinutes === 0;
}

function getTimeSpan() {
  return booking.endDate - booking.startDate;
}

function endTimeIsMultipleOf15Minutes() {
  return booking.endDate.getMinutes() % 15 === 0;
}

export default bookingValidator;