import bookingValidator from './bookingValidator.js';
import bookingPriceCalculator from './bookingPriceCalculator.js';
import bookingRates from '/scripts/data/rates.js';
import constants from '/scripts/data/constants.js';

var bookingService = {
  /**
   * 
   * @param {Array} bookings 
   */
  processAllBookings(bookings) {
    for (var i = 0; i < bookings.length; i++) {
      var booking = bookings[i];
      var formattedStartDate = booking["from"];
      var formattedEndDate = booking["to"];

      booking.isValid = _validate(formattedStartDate, formattedEndDate);
      booking.total = booking.isValid ? _getPrice(formattedStartDate, formattedEndDate, bookingRates) : 0;
    }
    return bookings;
  },

  /**
   * @param {string} formattedStartDate 
   * @param {string} formattedEndDate 
   * 
   * @returns A value that indicates wether the booking is valid.
   */
  validate(formattedStartDate, formattedEndDate) {
    return _validate(formattedStartDate, formattedEndDate);
  },

  getPrice(formattedStartDate, formattedEndDate, rates) {
    return _getPrice(formattedStartDate, formattedEndDate, rates);
  },
}

function _validate(formattedStartDate, formattedEndDate) {
  if (getTimezoneHourOffset(formattedStartDate) != getTimezoneHourOffset(formattedEndDate)) return false;

  var startDate = getDate(formattedStartDate);
  var endDate = getDate(formattedEndDate);
  return bookingValidator.validate(startDate, endDate);
}

function _getPrice(formattedStartDate, formattedEndDate, rates) {
  var startDate = getDate(formattedStartDate);
  var endDate = getDate(formattedEndDate);
  return bookingPriceCalculator.getPrice(startDate, endDate, rates);
}

function getDate(formattedDate) {
  var date = new Date(formattedDate);

  var utcDate = new Date(
    date.getUTCFullYear(),
    date.getUTCMonth(),
    date.getUTCDate(),
    date.getUTCHours(),
    date.getUTCMinutes(),
    date.getUTCSeconds(),
    date.getUTCMilliseconds());

  utcDate.setTime(utcDate.getTime() + getTimezoneHourOffset(formattedDate) * constants.TIMESPAN_OF_ONE_HOUR)

  return utcDate;
}

function getTimezoneHourOffset(formattedDate) {
  var regex = /([+-]\d\d?):00$/g;
  var match = regex.exec(formattedDate);
  return parseInt(match[1]);
}

export default bookingService;