import constants from '/scripts/data/constants.js';

var booking = {
  startDate: undefined,
  endDate: undefined
}

var bookingPriceCalculator = {
  getPrice(startDate, endDate, rates) {
    booking.startDate = startDate;
    booking.endDate = endDate;
    var ratePerHour = getRatePerHour(rates);
    var hours = getDurationInHours();
    var exactPrice = getPrice(ratePerHour, hours)
    return roundDown(exactPrice);
  }
}

function getRatePerHour(rates) {
  var highestRate = 0;
  for (var i = 0; i < rates.length; i++) {
    var rate = rates[i];
    var validRate = isValidRate(rate);
    if (validRate && rate.rate > highestRate) {
      highestRate = rate.rate;
    }
  }
  return highestRate;
}

function isValidRate(rate) {
  return isWithinTimeSpan(booking.startDate, rate) ||
    isWithinTimeSpan(booking.endDate, rate) ||
    wrapsTimeSpan(booking.startDate, booking.endDate, rate);
}

function isWithinTimeSpan(date, rate) {
  return containsDay(rate, date.getDay()) &&
    rate.startHour <= date.getHours() &&
    date.getHours() <= rate.endHour;
}

function wrapsTimeSpan(startDate, endDate, rate) {
  return (
    (
      (
        containsDay(rate, startDate.getDay()) ||
        containsDay(rate, getNextDayOfTheWeek(startDate.getDay()))
      ) &&
      rate.startHour >= startDate.getHours()
    ) && (
      (
        containsDay(rate, endDate.getDay()) ||
        containsDay(rate, getPreviousDayOfTheWeek(endDate.getDay()))
      ) &&
      rate.endHour <= endDate.getHours()
    )
  ) ||
  (
    getNextDayOfTheWeek(startDate.getDay()) === endDate.getDay() &&
    (
      (
        containsDay(rate, startDate.getDay()) &&
        rate.startHour > startDate.getHours()
      ) ||
      (
        containsDay(rate, endDate.getDay()) &&
        rate.endHour < endDate.getHours()
      )
    )
  )
}

function containsDay(rate, dayOfTheWeek) {
  return rate.days.indexOf(dayOfTheWeek) != -1;
}

function getNextDayOfTheWeek(dayOfTheWeek) {
  dayOfTheWeek++
  if (dayOfTheWeek === 8) {
    dayOfTheWeek = 1;
  }
  return dayOfTheWeek;
}

function getPreviousDayOfTheWeek(dayOfTheWeek) {
  dayOfTheWeek--
  if (dayOfTheWeek === 0) {
    dayOfTheWeek = 7;
  }
  return dayOfTheWeek;
}

function getDurationInHours() {
  return (booking.endDate - booking.startDate) / constants.TIMESPAN_OF_ONE_HOUR;
}

function getPrice(ratePerHour, hours) {
  return ratePerHour * hours;
}

function roundDown(price) {
  var regex = /\d+(\.\d{1,2})?/;
  var match = regex.exec(price);
  return parseFloat(match[0]);
}

export default bookingPriceCalculator;