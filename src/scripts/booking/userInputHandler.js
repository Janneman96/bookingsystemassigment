import bookingService from '/scripts/booking/bookingService.js';

var userInputHandler = {
    init() {
        var fileInput = document.querySelector('.js-bookings-upload');

        fileInput.addEventListener('change', function () {
            var textFile = fileInput.files[0];

            var reader = new FileReader();
            reader.readAsText(textFile);
            reader.onload = processFile;
        });
    }
}

function processFile(e) {
    var input = e.target.result;
    var bookings = JSON.parse(input);
    var result = bookingService.processAllBookings(bookings);

    var outputText = JSON.stringify(result, null, 4);
    downloadJsonFile(outputText);
}

function downloadJsonFile(content) {
    var dataStr = "data:text/json;charset=utf-8," + encodeURIComponent(content);
    var dlAnchorElem = document.createElement('a');
    dlAnchorElem.setAttribute("href", dataStr);
    dlAnchorElem.setAttribute("download", "output.json");
    dlAnchorElem.click();
    dlAnchorElem.remove();
}

export default userInputHandler;