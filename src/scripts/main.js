import userInputHandler from './booking/userInputHandler.js';
userInputHandler.init();

import testRunner from './tests/testrunner.js';
import bookingServiceTests from './tests/bookingServiceTests.js';
testRunner.runAll(bookingServiceTests);