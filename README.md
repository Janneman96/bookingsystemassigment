# Backend Assessment
## Overview
Voor deze opdracht willen we dat je een _calculator_ bouwt die kosten voor een boeking berekent. 
De applicatie die je gaat bouwen moet boekingen kunnen inlezen vanuit een JSON-bestand, hierop berekeningen loslaten en het resultaat naar een nieuw bestand schrijven.


## Input
We hebben een bestand `input.json` toegevoegd die een aantal _boekingen_ erin heeft staan. Elke boeking heeft de volgende waardes erin: booking-id, starttijd en eindtijd.

```json
[
  {
    "id": 1,
    "from": "2017-10-23T08:00:00+11:00",
    "to": "2017-10-23T11:00:00+11:00"
  },
  {
    "id": 2,
    "from": "2017-10-20T09:00:00+11:00",
    "to": "2017-10-20T11:45:00+11:00"
  }
]
```

## Regels

### Uurtarief 
De regels om het uurtarief te berekenen staan hieronder:

| Tarieftype | Van | Tot | Tarief (€) |
| -------- | ------- | -------- | ----------- |
| Dag  | 06:00 uur | 20:00 uur | 38,-
| Nacht | 20:00 uur | 06:00 uur | 42,93
| Zaterdag | Hele dag | Hele dag | 45,91
| Zondag | Hele dag | Hele dag | 60,85

### Regels
- Een boeking moet minimaal 1 uur zijn
- Een boeking mag maximaal 24 uur duren
- Een boeking kan niet starten voor de eindtijd
- Een boeking is altijd in eem meervoud van 15 minuten. dus 1600 tot 1715 bijvoorbeeld.
- Als een gedeelte van de boeking binnen het tarieftype 'nacht' valt, dan moet de volledige boeking op het nachttarief berekend worden.
  * Vrijdag 18:00 uur - 21:00 uur krijgt dus als berekening: 3 x 42,93
  * Woensdag 05:00 uur - 10:00 uur krijgt dus als berekening: 5 x 42,93
- Zaterdag- en zondagtarieven strekken over de hele dag, er is geen verschil tussen nacht en dag.

## Output
Jouw applicatie moet een bestand maken genaamd `output.json`. Hierin moet een array van boeking-objecten zitten met de waardes `isValid` en `total` toegevoegd. Als een boeking niet aan de regels voldoet dan moeten de waardes van `isValid` op false gezet worden en het totaal op 0.

```json
[
  {
    "id": 1,
    "from": "2017-10-23T08:00:00+11:00",
    "to": "2017-10-23T11:00:00+11:00",
    "isValid": true,
    "total": 114
  },
  {
    "id": 2,
    "from": "2017-10-20T09:00:00+11:00",
    "to": "2017-10-20T11:45:00+11:00" ,
    "isValid": true,
    "total": 104.50
  },
]
```
Het verwachte resultaat kan je vinden in het bestand `resultaat.json`.

## Succes!
Voor deze opdracht heb je ongeveer 8u. Je mag libraries gebruiken als je dat wilt. Voor vragen kan je meindert.hart@blink.nl mailen.